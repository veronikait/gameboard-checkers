/*
 * Copyright 2019 Veronika.com.ar (Rafael Benedettelli)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Random;

/**
 * Checked Board Game for Authority Partners. This game is build in context of
 * candidature for Authority Partners Organization. This is the third task of
 * the challenge in the Codity platform.
 *
 * All challenge is implemented in one class according to the Codity platform
 * evaluation. This task doesn't evaluate the skills over Oriented Object, so
 * all evaluation is implemented only using java structure language.
 *
 * For aboard the solution, the program is modularize in several methods for
 * code clarity and legibility logical flow.
 *
 * The class is unit logical component and anybody can run in any computer with
 * JDK+7. The only external packaged importer is java.util.Random for provide a
 * flexible and indeterminate scenarios for solution test.
 *
 * Assertions rules: - The boar is a squad matrix of {N,N} where N major than 10
 * and N minor than 30. The board has only one Jaffar pawn ('0') - Every board
 * line could has only one Spawn - The real execution game (parameter
 * SIMULATION_MODE = false) build a new random started scenario of NxN matrix
 * where the pawns appear in randomly boxes according the the rules. - Every *
 * execution game creates different scenarios. - We could make predictable test
 * * with the parameter SIMULATION_MODE = true building we own started scenario
 * in buildSquareSimulation() method.
 *
 * - The game has several basics initial parameters for make different type of
 * runs. - Any violation rule in configuration parameters will produce a
 * GameBoardException defined in this file.
 *
 * @author Rafael Benedettelli - Veronika.com.ar
 * @see GameBoardSimple#buildSquareSimulation()
 * @version 1.0
 */
public class GameBoardSimple {

    /**
     * Put this parameter in true for run a simulation mode according to the
     * matrix defined in BuildSquareSimulation() method or live in false to run
     * the game with unpredictable scenario build in randomly way.
     */
    private static final boolean SIMULATION_MODE = false;

    /**
     * Put this parameter in true for more verbose program. The program shows
     * all sequence of the Jaffar Spawn attacks.
     */
    private static final boolean SHOW_SEQUENCE_GRAPHIC = true;

    /**
     * When the SIMULATION_MODE is false, this value execute the integer number
     * of running games. The default value is 1 but we can change for a bigger
     * value if we want several game samples.
     */
    private static final int COUNT_GAMES_TO_PLAY = 1;

    /**
     * N is the board matrix dimension. The board follows an NxN square matrix.
     */
    private static final int N = 10;

    /**
     * The min dimension allowed by the Codity challenge
     */
    private static final int MIN_SQUAD_DIMENSION_ALLOWED = 10;

    /**
     * The max dimension allowed by the Codity challenge
     */
    private static final int MAX_SQUAD_DIMENSION_ALLOWED = 30;

    /**
     * The Pawn Jaffar visual representation.
     */
    private static final String PAWN_JAFFAR = "0";

    /**
     * The Pawn Aladin visual representation.
     */
    private static final String PAWN_ALADIN = "X";

    /**
     * The Empty Box visual representation.
     */
    private static final String EMPTY_BOX = ".";

    /**
     * The squareBoard String array like Codity propose for challenge.
     */
    private String[] squareBoard = new String[N];

    /**
     * Random helper for generate integer numbers in randomly way.
     */
    private static final Random RANDOM = new Random();

    /**
     * This is the main method. This is the entry point execution for run the
     * program.
     *
     * @param args (No line command arguments for this program)
     * @throws GameBoardException Any wrong parameter configuration may cause a
     * rule violation.
     */
    public static void main(String[] args) throws GameBoardException {

        System.out.println("Checkers Game Board!! Jaffar vs Aladin \n");
        System.out.println("===============================================\n");

        validateBoard();

        for (int i = 1; i <= COUNT_GAMES_TO_PLAY; i++) {

            System.out.println("\n\n............................................................" + i);
            System.out.println("Game number " + i);

            GameBoardSimple gameBoard = new GameBoardSimple();

            if (SIMULATION_MODE) {
                gameBoard.buildSquareSimulation();
            } else {
                gameBoard.buildSquareBoard();
            }

            System.out.println("::::Scenario");
            System.out.println(gameBoard.toString());

            int solution = gameBoard.solution(gameBoard.squareBoard);

            System.out.println("============================");
            System.out.println("Solution is: " + solution);

        }
    }

    /**
     * This method check all initial parameters and validate if there are any
     * violation rules.
     *
     * @throws GameBoardException if any violation rule happen.
     */
    private static void validateBoard() throws GameBoardException {

        if (!SIMULATION_MODE && COUNT_GAMES_TO_PLAY < 1) {

            throw new GameBoardException("The min count of play test for real game (no test) is 1 ");
        }

        if (N > MAX_SQUAD_DIMENSION_ALLOWED) {

            throw new GameBoardException("Max board dimension allowed is " + MAX_SQUAD_DIMENSION_ALLOWED);
        }

        if (N < MIN_SQUAD_DIMENSION_ALLOWED) {

            throw new GameBoardException("Min board dimension allowed is " + MIN_SQUAD_DIMENSION_ALLOWED);
        }

    }

    /**
     * This method allows design own started scenario for test. This testing
     * scenario only is created when SIMULATION_MODE parameter is true.
     */
    public void buildSquareSimulation() {

        final int N_FOR_SIMULATION = 10;

        squareBoard = new String[N_FOR_SIMULATION];

        squareBoard[0] = "X.........";
        squareBoard[1] = "X.........";
        squareBoard[2] = "...X......";
        squareBoard[3] = "X.........";
        squareBoard[4] = "...X......";
        squareBoard[5] = "X.........";
        squareBoard[6] = "...X......";
        squareBoard[7] = "X.........";
        squareBoard[8] = ".X........";
        squareBoard[9] = "0.........";

    }

    /**
     * Generate a new Square board in randomly way. This board is only created
     * when SIMULATION_MODE is false.
     */
    public void buildSquareBoard() {

        for (int i = 0; i < N; i++) {

            squareBoard[i] = getFieldLine(getRandomPawnPosition());

        }

        putJaffarPawnRandom();
    }

    /**
     * This method put the only Jaffar Pawn in the game in a random raw and
     * column position over the board.
     */
    public void putJaffarPawnRandom() {

        int jaffarPostionColumn = getRandomPawnPosition();
        int jaffarPositionRaw = getRandomPawnPosition();

        String field = "";

        for (int i = 0; i < N; i++) {

            if (jaffarPostionColumn == i) {
                field += getJafarPawn();
            } else {
                field += EMPTY_BOX;
            }
        }

        //Replaces for line with Jaffar Pawin
        squareBoard[jaffarPositionRaw] = field;

    }

    /**
     * This method get the entire raw String line of the pawn column position in
     * the board.
     *
     * @param pawnColumnPosition the columnPawn position in the board.
     * @return the String line of pawnColumnPosition
     */
    private String getFieldLine(int pawnColumnPosition) {

        String field = "";

        for (int i = 0; i < N; i++) {

            if (pawnColumnPosition == i) {
                field += getAladinPawn();
            } else {
                field += EMPTY_BOX;
            }
        }

        return field;
    }

    /**
     * This method get the Aladin pawn.
     *
     * @return The Aladin pawn String representation.
     */
    private static String getAladinPawn() {

        return PAWN_ALADIN;
    }

    /**
     * This method get the Jaffar pawn.
     *
     * @return The Jaffar pawn String representation.
     */
    private static String getJafarPawn() {

        return PAWN_JAFFAR;
    }

    /**
     * This method return a random number position box for put a pawn.
     *
     * @return random position of the box.
     */
    private int getRandomPawnPosition() {

        int position = RANDOM.nextInt(N);

        return position;
    }

    /**
     * The solution method for the GameBoardSimple.
     *
     * @param B the board game represented by array of Strings.
     * @return the number of max Aladin pawn beaten by Jaffar.
     */
    public int solution(String[] B) {

        final int MIN_SQUAD_DIMENSION_FOR_BEAT = 3;

        int aladinPawnsBeaten = 0;

        if (B.length >= MIN_SQUAD_DIMENSION_FOR_BEAT) {

            while (canJaffarBeatPawn(B)) {

                aladinPawnsBeaten++;

                if (SHOW_SEQUENCE_GRAPHIC) {

                    System.out.println("::::Sequence " + aladinPawnsBeaten);
                    System.out.println(this.toString());
                    System.out.println("     ||    ");
                    System.out.println("     ||    ");
                    System.out.println("     \\/  \n");

                }

            }

        }

        return aladinPawnsBeaten;

    }

    /**
     * This method indicate if Jaffar can follow beating.
     *
     * @param B the game board represented by String array.
     * @return true if Jaffar could beat or false in otherwise.
     */
    public boolean canJaffarBeatPawn(String[] B) {

        int positionRawJaffarPawn = getRawNumberOfJaffarPawn(B);
        int positionColumnJaffarPawn = getColumnPositionForPawn(B[positionRawJaffarPawn], PAWN_JAFFAR.charAt(0));

        if (positionRawJaffarPawn > 1) {

            int nextRaw = positionRawJaffarPawn - 1;
            int newRawJaffarPawn = nextRaw - 1;

            int positionColumnAladinPawn = getColumnPositionForPawn(B[nextRaw], PAWN_ALADIN.charAt(0));

            //3 conditions
            //a. The Aladin position is equals to jaffarPostion -+ 1
            //a. The columnAladinPostion 
            boolean canBeatLeft = positionColumnAladinPawn == (positionColumnJaffarPawn - 1);
            boolean canBeatRigth = positionColumnAladinPawn == (positionColumnJaffarPawn + 1);

            //Test for left
            if (canBeatLeft) {

                if (positionColumnAladinPawn > 0) {

                    if (isEmptyBox(B[newRawJaffarPawn], (positionColumnAladinPawn - 1))) {

                        putJaffarPawnNewPosition(B, newRawJaffarPawn, positionColumnAladinPawn - 1);

                        return true;
                    }

                }

            }

            //test for right
            if (canBeatRigth) {

                if (positionColumnAladinPawn < (B.length - 1)) {

                    if (isEmptyBox(B[newRawJaffarPawn], (positionColumnAladinPawn + 1))) {

                        putJaffarPawnNewPosition(B, newRawJaffarPawn, positionColumnAladinPawn + 1);

                        return true;
                    }

                }

            }

        }

        return false;
    }

    /**
     * Return the Line number where is the JAFFAR pawn. The game build assumes
     * that Jaffar pawns share the same line and column number. For example, if
     * the line number is 4, the column number is 4 too.
     *
     * @param B the array with the game board.
     * @return the exact line number with the Jaffar pawns (and is the column
     * number too).
     */
    public int getRawNumberOfJaffarPawn(String[] B) {

        for (int i = 0; i < B.length; i++) {

            if (B[i].contains(PAWN_JAFFAR)) {

                return i;
            }

        }
        return -1;
    }

    /**
     * This helper method returns the box position number in
     * the board of the one row for the indicate pawn.
     * @param line the raw line in the board.
     * @param pawn the Jaffar or Aladin pawn,
     * @return the correct position or -1 if there is error.
     */
    private int getColumnPositionForPawn(String line, char pawn) {

        char[] arrayChar = line.toCharArray();

        for (int j = 0; j < arrayChar.length; j++) {

            if (arrayChar[j] == pawn) {

                return j;
            }

        }
        return -1;
    }

    /**
     * Indicate if the coordinates for (line,column) is empty in the board.
     *
     * @param line
     * @param column
     * @return
     */
    private boolean isEmptyBox(String line, int column) {

        char box = line.charAt(column);

        return box == EMPTY_BOX.charAt(0);

    }

    private void putJaffarPawnNewPosition(String B[], int line, int column) {

        char charArray[] = B[line].toCharArray();

        charArray[column] = PAWN_JAFFAR.charAt(0);

        String updatedLine = new String(charArray);

        B[line] = updatedLine;
        B[line + 1] = B[line + 1].replace(PAWN_ALADIN, EMPTY_BOX);
        B[line + 2] = B[line + 2].replace(PAWN_JAFFAR, EMPTY_BOX);

    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();

        for (String fieldLine : squareBoard) {

            builder.append(fieldLine);
            builder.append("\n");

        }

        return builder.toString();
    }

}

class GameBoardException extends Exception {

    public GameBoardException(String message) {

        super(message);
    }

}
