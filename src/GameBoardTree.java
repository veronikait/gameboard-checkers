/*
 * Copyright 2019 Veronika.com.ar (Rafael Benedettelli)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Random;

/**
 * Checked Board Game for Authority Partners. This game is build in context of
 * candidature for Authority Partners Organization. This is the third task of
 * the challenge in the Codity platform.
 *
 * All challenge is implemented in one class according to the Codity platform
 * evaluation. This task doesn't evaluate the skills over Oriented Object, so
 * all evaluation is implemented only using java structure language.
 *
 * For aboard the solution, the program is modularize in several methods for
 * code clarity and legibility logical flow.
 *
 * The class is unit logical component and anybody can run in any computer with
 * JDK+7. The only external packaged importer is java.util.Random for provide a
 * flexible and indeterminate scenarios for solution test.
 *
 * Assertions rules: - The boar is a squad matrix of {N,N} where N major than 10
 * and N minor than 30
 *
 * - The board has only one Jaffar pawn ('0')
 *
 * - Every board line could has only one Spawn
 *
 * - The real execution game (parameter SIMULATION_MODE = false) build a new
 * random started scenario of NxN matrix where the pawns appear in randomly
 * boxes according the the rules.
 *
 * - Every execution game creates different scenarios. - We could make
 * predictable test * with the parameter SIMULATION_MODE = true building we own
 * started scenario in buildSquareSimulation() method.
 *
 * - The game has several basics initial parameters for make different type of
 * runs.
 *
 * - Any violation rule in configuration parameters will produce a
 * GameBoardException defined in this file.
 *
 * @author Rafael Benedettelli - Veronika.com.ar
 * @see GameBoard#buildSquareSimulation()
 * @version 1.0
 */
public class GameBoardTree {

    /**
     * *********************************************************
     *
     * Program parameters
     *
     ***********************************************************
     */
    /**
     * Put this parameter in true for run a simulation mode according to the
     * matrix defined in BuildSquareSimulation() method or live in false to run
     * the game with unpredictable scenario build in randomly way.
     */
    private static final boolean SIMULATION_MODE = true;

    /**
     * When the SIMULATION_MODE is false, this value execute the integer number
     * of running games. The default value is 1 but we can change for a bigger
     * value if we want several game samples.
     */
    private static final int COUNT_GAMES_TO_PLAY = 1;

    /**
     * N is the board matrix dimension. The board follows an NxN square matrix.
     */
    private static final int N = 10;

    /**
     * The min dimension allowed by the Codity challenge
     */
    private static final int MIN_SQUAD_DIMENSION_ALLOWED = 10;

    /**
     * The max dimension allowed by the Codity challenge
     */
    private static final int MAX_SQUAD_DIMENSION_ALLOWED = 30;

    /**
     * *********************************************************
     *
     * Fields for program
     *
     ***********************************************************
     */
    /**
     * The Pawn Jaffar visual representation.
     */
    private static final String PAWN_JAFFAR = "0";

    /**
     * The Pawn Aladin visual representation.
     */
    private static final String PAWN_ALADIN = "X";

    /**
     * The Empty Box visual representation.
     */
    private static final String EMPTY_BOX = ".";

    /**
     * The squareBoard String array like Codity propose for challenge.
     */
    private String[] squareBoard = new String[N];

    /**
     * Random helper for generate integer numbers in randomly way.
     */
    private static final Random RANDOM = new Random();

    /**
     * This is the main method. This is the entry point execution for run the
     * program.
     *
     * @param args (No line command arguments for this program)
     * @throws GameBoardExceptionTree Any wrong parameter configuration may
     * cause a rule violation.
     */
    public static void main(String[] args) throws GameBoardExceptionTree {

        System.out.println("Checkers Game Board!! Jaffar vs Aladin \n");
        System.out.println("===============================================\n");

        validateBoard();

        for (int i = 1; i <= COUNT_GAMES_TO_PLAY; i++) {

            System.out.println("\n\n............................................................");
            System.out.println("Game number " + i);

            GameBoardTree gameBoard = new GameBoardTree();

            if (SIMULATION_MODE) {
                gameBoard.buildSquareSimulation();
            } else {
                gameBoard.buildSquareBoard();
            }

            System.out.println("::::Scenario");
            System.out.println(gameBoard.toString());

            
            
            long startTime = System.nanoTime();
            int solution = new Solution().solution(gameBoard.squareBoard);            
            long finishTime = System.nanoTime() - startTime;
            

            System.out.println("============================");
            
            System.out.println("Solution is: " + solution);
            System.out.println("Total time in nanos: " + finishTime);

        }
    }

    /**
     * This method check all initial parameters and validate if there are any
     * violation rules.
     *
     * @throws GameBoardException if any violation rule happen.
     */
    private static void validateBoard() throws GameBoardExceptionTree {

        if (!SIMULATION_MODE && COUNT_GAMES_TO_PLAY < 1) {

            throw new GameBoardExceptionTree("The min count of play test for real game (no test) is 1 ");
        }

        if (N > MAX_SQUAD_DIMENSION_ALLOWED) {

            throw new GameBoardExceptionTree("Max board dimension allowed is " + MAX_SQUAD_DIMENSION_ALLOWED);
        }

        if (N < MIN_SQUAD_DIMENSION_ALLOWED) {

            throw new GameBoardExceptionTree("Min board dimension allowed is " + MIN_SQUAD_DIMENSION_ALLOWED);
        }

    }

    /**
     * This method allows design own started scenario for test. This testing
     * scenario only is created when SIMULATION_MODE parameter is true.
     */
    public void buildSquareSimulation() {

        final int N_FOR_SIMULATION = 20;

        squareBoard = new String[N_FOR_SIMULATION];

        squareBoard[0] = "....................";
        squareBoard[1] = "....................";
        squareBoard[2] = "....................";
        squareBoard[3] = "....................";
        squareBoard[4] = "...X................";
        squareBoard[5] = "....................";
        squareBoard[6] = "...X...X............";
        squareBoard[7] = "....................";
        squareBoard[8] = ".....X.........XX...";
        squareBoard[9] = "....................";
        squareBoard[10] = "...X.X.......X..X...";
        squareBoard[11] = "..............X.....";
        squareBoard[12] = "...X.X.....X........";
        squareBoard[13] = "....................";
        squareBoard[14] = "...X.X...X..........";
        squareBoard[15] = "....................";
        squareBoard[16] = "...X.X.X............";
        squareBoard[17] = "....................";
        squareBoard[18] = "...X.X..............";
        squareBoard[19] = "....0...............";

    }

    /**
     * Generate a new Square board in randomly way. This board is only created
     * when SIMULATION_MODE is false.
     */
    public void buildSquareBoard() {

        for (int i = 0; i < N; i++) {

            squareBoard[i] = getFieldLine();

        }

        putJaffarPawnRandom();

    }

    /**
     * This method put the only Jaffar Pawn in the game in a random raw and
     * column position over the board.
     */
    public void putJaffarPawnRandom() {

        int jaffarPostionColumn = getRandomPawnPosition();
        int jaffarPositionRaw = getRandomPawnPosition();

        String field = "";

        for (int i = 0; i < N; i++) {

            if (jaffarPostionColumn == i) {
                field += getJafarPawn();
            } else {
                field += EMPTY_BOX;
            }
        }

        //Replaces for line with Jaffar Pawin
        squareBoard[jaffarPositionRaw] = field;

    }

    /**
     * This method get the entire raw String line of the pawn column position in
     * the board.
     *
     * @param pawnColumnPosition the columnPawn position in the board.
     * @return the String line of pawnColumnPosition
     */
    private String getFieldLine() {

        String field = "";

        for (int i = 0; i < N; i++) {

            if (isAladinPawn()) {
                field += getAladinPawn();
            } else {
                field += EMPTY_BOX;
            }
        }

        return field;
    }

    /**
     * This method determine if put or not a pawn of Aladin in the current box.
     *
     * @return true if put Aladin pawn and false in otherwise.
     */
    private boolean isAladinPawn() {

        return RANDOM.nextBoolean();

    }

    /**
     * This method get the Aladin pawn.
     *
     * @return The Aladin pawn String representation.
     */
    private static String getAladinPawn() {

        return PAWN_ALADIN;
    }

    /**
     * This method get the Jaffar pawn.
     *
     * @return The Jaffar pawn String representation.
     */
    private static String getJafarPawn() {

        return PAWN_JAFFAR;
    }

    /**
     * get box valid random position for pawn position on the board.
     *
     * @return the number of box in the board.
     */
    private int getRandomPawnPosition() {

        int position = RANDOM.nextInt(N);

        return position;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();

        for (String fieldLine : squareBoard) {

            builder.append(fieldLine);
            builder.append("\n");

        }

        return builder.toString();
    }

    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
     */
    static class GameBoardExceptionTree extends Exception {

        public GameBoardExceptionTree(String message) {

            super(message);
        }

    }

}
