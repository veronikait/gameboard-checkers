/*
 * Copyright 2019 Veronika.com.ar (Rafael Benedettelli)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Checked Board Game for Authority Partners. This game is build in context of
 * candidature for Authority Partners Organization. This is the third task of
 * the challenge in the Codity platform.
 *
 * The extra-challenge was implement all solution in only one method
 * whit no more than a few class field attributes to help maintain the state
 * over the recursive iteration.
 *
 * The solution was based on drill the tree for find
 * the best road of all possible.
 *
 * Assertions rules:
 * *****************
 *  A. The boar is a squad matrix of {N,N} where N is major than 10 and N minor than 30.
 *  B. The game has only one Jaffar pawn ('0') in all the board.
 *  C. Every board line could has a multiple Aladin Spawn by raw. (This is the advance version).
 *
 *
 * @author Rafael Benedettelli - Veronika.com.ar (rblbene@gmail.com)
 * @see GameBoard#buildSquareSimulation()
 * @version 1.0
 */
public class Solution {

    /**
     * This field indicate if the solution it's starting for recognize the
     * initial Jaffar pawn position on the board.
     */
    private boolean isInit = true;

    /**
     * This field indicate if program is drilling by left or right side for
     * explore all possible roads.
     */
    private boolean isDrillByLeft = true;

    /**
     * Field for save the initial position of Jaffar pawn on the board.
     */
    private int initialRawJaffarPawn;

    /**
     * Field for save the current raw position of Jaffar pawn on the board while
     * the program is drilling the tree.
     */
    private int currentRawJaffarPawn;

    /**
     * Field for save the current column position of Jaffar pawn on the board
     * while the program is drilling the tree.
     */
    private int currentColumnJaffarPawn;

    /**
     * The board scenario dimension.
     */
    private int N;

    /**
     * The minimal boar dimension for play the game.
     */
    private static final int MIN_BOARD_DIMENSION = 10;

    /**
     * The maxim boar dimension for play the game.
     */
    private static final int MAX_BOARD_DIMENSION = 30;

    /**
     * Constant for Jaffar Pawn visual representation.
     */
    private static final char PAWN_JAFFAR = '0';

    /**
     * Constant for Jaffar Pawn visual representation.
     */
    private static final char PAWN_ALADIN = 'X';

    /**
     * Constant for Jaffar Pawn visual representation.
     */
    private static final char EMPTY_BOX = '.';

    /**
     * The max position reached by Jaffar pawn on the board.
     */
    private int maxPositionReached = Integer.MAX_VALUE;

    /**
     * This method implement all solution for the game. It's very important
     * clear out that the implementations not follows the "BEST" designed
     * solution but the challenge it's focus in the algorithmic skills than the
     * Oriented-Design Software practices.
     * 
     * The algorithmic strategy for resolve the problem is the iterate recursively
     * over the Binary tree to find the largest road.
     *
     * The extra-challenge for me was implement all solution in only one
     * recursive method. The original idea was resolve the problem in the minor
     * quantity of code possible.
     *
     * For resolve the problem was required 51 effective code lines
     * (This 51 lines not included method signature, class attributes and
     * documentation).
     *
     * @param B the array of String that represents the board game.
     * @return The solution of the game based on the max Aladin pawn eat by
     * Jaffar. If some validation error happen, the method will return -1
     * standard error code.
     *
     */
    public int solution(String[] B) {

        //Only for firt time to detect the Jaffar pawn position on the board.
        if (isInit) {

            N = B.length;
            //Validate N
            if (N < MIN_BOARD_DIMENSION || N > MAX_BOARD_DIMENSION) {
                System.err.println("Board error: dimension out of range allowed");
                return -1;
            }

            for (int i = 0; i < N; i++) {

                if (B[i].contains(String.valueOf(PAWN_JAFFAR))) {

                    initialRawJaffarPawn = currentRawJaffarPawn = i;

                    currentColumnJaffarPawn = B[i].indexOf(PAWN_JAFFAR);
                }
            }

            isInit = false;
        }
        //----------------------------------------------------------------------------

        //Local constant for diagonal movement 
        final int EAT_MOVEMENT = 2;

        //Indicate if jaffar pawn can eat Aladin pawn.
        boolean canEat = false;

        //1. First condition: check if Jaffar is not on the top of the board and has handicap for eat a pawn.
        //                          [ ][ ][ ][ ][ ]
        //   Sample Right scenario: [ ][ ][ ][ ][ ]
        //                          [ ][ ][0][ ][ ]
        boolean jaffarPawnHasMinRowRangeToEat = currentRawJaffarPawn > 1;

        if (jaffarPawnHasMinRowRangeToEat) {

            //The program will explore by left or by rigth.
            //Move one box left or rigth. 
            int offsetColum = (isDrillByLeft) ? -1 : 1;

            int nextRaw = currentRawJaffarPawn - 1;

            //The new Jaffar pawn position if can eat Aladin pawn.
            int newRawJaffarPawn = nextRaw - 1;

            //2. Second condition: check if Jaffar has two more columnsat both sides from rigth or left.
            //                          [ ][ ][ ][ ][ ]
            //   Sample Right scenario: [ ][ ][ ][ ][ ]
            //                          [ ][ ][0][ ][ ]        
            boolean isNextAttachInsideOfBoard = isDrillByLeft && currentColumnJaffarPawn > 1;
            isNextAttachInsideOfBoard = isNextAttachInsideOfBoard || !isDrillByLeft && currentColumnJaffarPawn < (N - 2);

            if (isNextAttachInsideOfBoard) {

                boolean isAladinPawnInDiagonal = B[nextRaw].charAt(currentColumnJaffarPawn + offsetColum) == PAWN_ALADIN;

                //3. Third condition: check if Jaffar pawn has each Aladin pawn at diagonal (Rigth or left.)
                //The Sample scenario shows Aladin pawn at left side.
                //                          [ ][ ][ ][ ][ ]
                //   Sample Right scenario: [ ][X][ ][ ][ ]
                //                          [ ][ ][0][ ][ ]
                if (isAladinPawnInDiagonal) {

                    int columnAladinPawn = currentColumnJaffarPawn + offsetColum;

                    char nextJaffarBox = B[newRawJaffarPawn].charAt(columnAladinPawn + offsetColum);

                    boolean isNextJaffarPositionEmptyBox = nextJaffarBox == EMPTY_BOX;

                    //4. Fourd condition: check if Jaffar pawn has empty box at two diagonal steps.
                    //The Sample scenario shows Aladin pawn at left side.
                    //                         [.][ ][ ][ ][ ]
                    //  Sample Right scenario: [ ][X][ ][ ][ ]
                    //                         [ ][ ][0][ ][ ]
                    canEat = isNextJaffarPositionEmptyBox;

                }
            }

            //The Jaffar Pawn can eat becouse suplly with the four conditions
            if (canEat) {

                //Update the jaffar pawn position and move up forward on the board.
                currentRawJaffarPawn -= EAT_MOVEMENT; //Always up direction
                currentColumnJaffarPawn += (offsetColum * EAT_MOVEMENT); //Left or rigth

                //Save the max raw position reached by Jaffar pawn.
                if (currentRawJaffarPawn < maxPositionReached) {

                    maxPositionReached = currentRawJaffarPawn;

                }

                //still drilling by left side
                isDrillByLeft = true;
                solution(B);

                //Always that Jaffar could eat the pawn debt two position.
                //This fragment of code back Jaffar pawn two steps on the board.
                currentRawJaffarPawn += EAT_MOVEMENT;
                currentColumnJaffarPawn += EAT_MOVEMENT;

                //Indicate that the next direction will be left
                isDrillByLeft = true;

            }
        }

        if (isDrillByLeft) {

            isDrillByLeft = false;
            solution(B);

        }

        //The solution calculus reverse equation
        int solution = (maxPositionReached < N) ? ((initialRawJaffarPawn - (maxPositionReached)) / 2) : 0;
        
        
        return solution;

    }

}
