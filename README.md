# Java Checkers Gameboard #

Implementation algorithm for checkers gameboard. This software implements a search in binary tree for choose the best play of all posible roads
on the gameboard.


 The extra-challenge was implement all solution in only one method  whit no more than a few class field attributes to help maintain the state
 over the recursive iteration.

 The solution was based on drill the tree for find the best road of all possible.

## Assertions rules ##

  A. The boar is a squad matrix of {N,N} where N is major than 10 and N minor than 30.
  B. The game has only one Jaffar pawn ('0') in all the board.
  C. Every board line could has a multiple Aladin Spawn by raw. (This is the advance version).

## Specifications ##
Java 11

## Run it ##
just execute: 
     
           java GameBoard


## Author ##

Ing. Rafael Benedettelli

veronika.com.ar

rblbene@gmail.com


